CONTENTS OF THIS FILE
---------------------
* Introduction
* Requirements
* Installation
* Configuration
* Usage
* Maintainers

INTRODUCTION
------------
NSE Stocks modules support displaying the stock information as a block.
NSE Stocks is an extremely lightweight module designed to display
NSE stock information. The module provides block to place NSE stock details
based on the NSE code when place a block. This module provides option to refresh
stock information with auto refresh.

* For a full description of the module, visit the project page:
  https://www.drupal.org/project/nse_stocks

* To submit bug reports and feature suggestions, or track changes:
  https://www.drupal.org/project/issues/nse_stocks

REQUIREMENTS
------------
This module requires no modules outside of Drupal core. Get API from https://stockmarketapi.in/

INSTALLATION
------------
* Install as you would normally install a contributed Drupal module. Visit
  https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------
* Enter the Stock API configuration: Administration »
  Configuration » Web services » NSE Stocks » NSE Stocks Configuration.

* Configure the user permissions in Administration » People » Permissions:

    - Administer NSE Stocks configuration

USAGE
-----
* Display stock for a particular company by using NSE Code.
  Please follow the steps to display via block.

  * Administration » Structure » Block layout » Place block »
  Find "NSE Stock Information Display" under "NSE Stocks" category.
  * Enter NSE code, Date Format, Auto Refresh in Seconds and Save block

MAINTAINERS
-----------
Current maintainers:
* Selvakumar Thangavelu [selvakumar] - https://www.drupal.org/u/selvakumar
* Iyyappan Matheri Govindasamy [iyyappan-govind] - https://www.drupal.org/u/iyyappangovind-0
