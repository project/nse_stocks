/**
 * @file
 * Behaviors for the NSE Stocks market.
 * This script will update the NSE Stock market price at configured seconds.
 */

(function ($, Drupal, drupalSettings) {
    Drupal.behaviors.nse_stocks = {
        attach: function(context) {
            $(document).ready(function() {
                $(".nse-stock-info").each(function(){
                    var refreshSeconds = $(this).data("autorefresh");
                    var nseCode = $(this).data("nsecode");
                    var dateTimeFormat = $(this).data("date_time_format");
                    var path = "/nse_stocks/stock_information";
                    if(refreshSeconds != 0 || refreshSeconds != "") {
                        setInterval(function (context) {
                            $.ajax({
                                url: path,
                                method: 'GET',
                                data: {
                                    nseCode: nseCode,
                                    refreshSeconds: refreshSeconds,
                                    dateTimeFormat: dateTimeFormat,
                                },
                            }).done(function (data) {
                                var tagerId = "stock_market_data_"+nseCode;
                                $("#"+tagerId).html(data.data);
                            });
                        }, refreshSeconds);
                    }
                });
            });
        }
    }
}(jQuery, Drupal, drupalSettings));
