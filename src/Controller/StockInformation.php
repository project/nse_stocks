<?php

namespace Drupal\nse_stocks\Controller;

/**
 * To refresh NSE stock information automated.
 */

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Language\LanguageManagerInterface;
use GuzzleHttp\ClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Render\RendererInterface;

/**
 * Description of CaptchaImageRefresh.
 */
class StockInformation extends ControllerBase {

  /**
   * Language Manager.
   *
   * @var Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The HTTP client.
   *
   * @var GuzzleHttp\ClientInterface
   */
  protected ClientInterface $httpClient;

  /**
   * The request stack.
   *
   * @var Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Drupal\Core\Render\RendererInterface definition.
   *
   * @var Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Constructor with Dependency Injection.
   *
   * @inheritdoc
   */
  public function __construct(LanguageManagerInterface $language_manager, ConfigFactoryInterface $config_factory,
                              ClientInterface $httpClient, RequestStack $request_stack, RendererInterface $renderer)
  {
    $this->languageManager = $language_manager;
    $this->nseStocksConfig = $config_factory->getEditable('nse_stocks.stock_configurations');
    $this->httpClient = $httpClient;
    $this->requestStack = $request_stack;
    $this->renderer = $renderer;
  }
  /**
   * Function create.
   *
   * @param Symfony\Component\DependencyInjection\ContainerInterface $container
   *   Container interface.
   *
   * @return static
   */
  public static function create(ContainerInterface $container)
  {
    return new static(
        $container->get('language_manager'),
        $container->get('config.factory'),
        $container->get('http_client'),
        $container->get('request_stack'),
        $container->get('renderer')
    );
  }

  /**
   * Put your code here.
   */
  public function refreshStockInfo() {
    $output = [];
    $apiKey = $this->nseStocksConfig->get('api_key');
    $nseCode = $this->requestStack->getCurrentRequest()->get('nseCode');
    $refreshSeconds = $this->requestStack->getCurrentRequest()->get('refreshSeconds');
    $dateTimeFormat = $this->requestStack->getCurrentRequest()->get('dateTimeFormat');

    $apiUrl = "https://api.stockmarketapi.in/api/v1/getprices?token=".$apiKey."&nsecode=".$nseCode;
    $output["time"] = date($dateTimeFormat, time());
    $output["nsecode"] = $nseCode;
    $output["refresh"] = $refreshSeconds;
    // Try to get the NSE Stock based on input.
    try {
      $response = $this->httpClient->request("GET", $apiUrl, ['headers' => ['content-type' => 'application/json']]);
      if ($response->getStatusCode() == 200) {
        $data = json_decode($response->getBody()->getContents());
        $output["status"] = "Success";
        $output["response"] = $data->data->$nseCode;
      }
    }
    catch (RequestException $e) {
      $output["status"] = "Error";
    }
    $content = [
        '#theme' => 'nse_stocks_block',
        '#items' => $output,
    ];
    $outputHtml = $this->renderer->render($content);
    return new JsonResponse([ 'data' => $outputHtml, 'method' => 'GET', 'status'=> 200]);
  }

}
