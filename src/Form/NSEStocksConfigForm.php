<?php

namespace Drupal\nse_stocks\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Class NSEStocksConfigForm.
 *
 * Form to config NSE Stocks API and supported configurations.
 */
class NSEStocksConfigForm extends ConfigFormBase {

  /**
   * NSE Stocks Config.
   *
   * @var mixed
   */
  protected $nseStocksConfig;

  /**
   * Config Factory.
   *
   * @var Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->nseStocksConfig = $config_factory->getEditable('nse_stocks.stock_configurations');
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')
    );
  }

  /**
   * Get the form id.
   *
   * @return string
   *   Unique form id.
   */
  public function getFormId() {
    return 'nse_stocks_configurations_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'nse_stocks.stock_configurations',
    ];
  }

  /**
   * Generate the full Form Element.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   FormState Object.
   *
   * @return array
   *   Return Form Element.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['nse_stocks_config'] = [
      '#title' => $this->t('NSE Configurations'),
      '#type' => 'fieldset',
    ];

    $form['nse_stocks_config']['api_key'] = [
      '#type' => 'textarea',
      '#required' => TRUE,
      '#title' => $this->t('Enter API Key'),
      '#description' => $this->t('Please enter stock market api key. Go to <a href="https://stockmarketapi.in/">https://stockmarketapi.in/</a> and get the API key.'),
      '#default_value' => $this->nseStocksConfig->get('api_key'),
       '#rows' => 1,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->nseStocksConfig->set("api_key", $form_state->getValue("api_key"))->save();
    parent::submitForm($form, $form_state);
  }

}
