<?php

namespace Drupal\nse_stocks\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;


/**
 * Class NSEStocksBlock.
 *
 * Provides a NSE Stock Information.
 *
 * @Block(
 *  id = "nse_stocks",
 *  admin_label = @Translation("NSE Stock Information Display"),
 * )
 */
class NSEStocksBlock extends BlockBase implements ContainerFactoryPluginInterface
{

  /**
   * Language Manager.
   *
   * @var Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The http client.
   *
   * @var GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Constructor with Dependency Injection.
   *
   * @inheritdoc
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, LanguageManagerInterface $language_manager, ConfigFactoryInterface $config_factory, ClientInterface $httpClient)
  {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->languageManager = $language_manager;
    $this->nseStocksConfig = $config_factory->getEditable('nse_stocks.stock_configurations');
    $this->httpClient = $httpClient;
  }

  /**
   * Function create.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   Container interface.
   * @param array $configuration
   *   Block Configuration.
   * @param string $plugin_id
   *   Block Plugin id.
   * @param mixed $plugin_definition
   *   Block Definition.
   *
   * @return static
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition)
  {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('language_manager'),
      $container->get('config.factory'),
      $container->get('http_client')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();
    $form['nsecode'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('NSE Code'),
      '#description' => $this->t('Enter the NSE Code for the display the stock information.'),
      '#default_value' => $config['nsecode'],
    ];
    $form['date_time_format'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Date Format'),
        '#maxlength' => 100,
        '#description' => $this->t('A user-defined date format. See the <a href="https://www.php.net/manual/datetime.format.php#refsect1-datetime.format-parameters" target="_blank">PHP manual</a> for available options.'),
        '#required' => TRUE,
        '#default_value' => $config['date_time_format'],
    ];
    $form['refresh_seconds'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Auto Refresh in Seconds'),
        '#description' => $this->t('Enter the seconds to auto refresh the stock information. Ex: 2000 for 2 seconds.
            Auto refresh not happened if it is empty.'),
        '#default_value' => $config['refresh_seconds'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->setConfigurationValue('nsecode', $form_state->getValue('nsecode'));
    $this->setConfigurationValue('date_time_format', $form_state->getValue('date_time_format'));
    $this->setConfigurationValue('refresh_seconds', $form_state->getValue('refresh_seconds'));
  }

  /**
   * Function build.
   *
   * @return array
   *   The API response for Best selling phones
   */
  public function build() {
    $output = [];
    $config = $this->getConfiguration();
    $apiKey = $this->nseStocksConfig->get('api_key');
    $nseCode = $config['nsecode'];
    $refreshSeconds = $config['refresh_seconds'];
    $dateTimeFormat = $config['date_time_format'];

    $apiUrl = "https://api.stockmarketapi.in/api/v1/getprices?token=".$apiKey."&nsecode=".$nseCode;
    $output["time"] = date($config["date_time_format"], time());
    $output["nsecode"] = $nseCode;
    $output["refresh"] = $refreshSeconds;
    $output["date_time_format"] = $dateTimeFormat;
    // Try to get the NSE Stock based on input.
    try {
      $response = $this->httpClient->request("GET", $apiUrl, ['headers' => ['content-type' => 'application/json']]);
      if ($response->getStatusCode() == 200) {
        $data = json_decode($response->getBody()->getContents());
        $output["status"] = "Success";
        $output["response"] = $data->data->$nseCode;
      }
    }
    catch (RequestException $e) {
      $output["status"] = "Error";
    }

    return [
      '#theme' => 'nse_stocks_block',
      '#items' => $output,
      '#attached' => [
        'library' => [
          'nse_stocks/stocks',
        ],
      ],
      '#prefix' => '<div id="stock_market_data_'.$nseCode.'">',
      '#suffix' => '</div>',
      '#cache' => [
        'max-age' => 0,
      ]
    ];
  }
}
